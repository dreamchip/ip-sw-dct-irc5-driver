# Interrupt Controller with 5-Register Interface

This repository contains drivers for the IRC5 interrupt controller from Dream Chip.

Currently only a Linux kernel module is available.


[TOC]


## Build, Deploy & Test

### Native Compile

To build the Linux kernel module a path to the kernel sources must be provided, afterwards

```sh
cd <clone-dir>/linux
export KERNEL_SRC=/usr/src/linux-headers-`uname -r`
make
```

### Cross-Compile

If you want to cross-compile using a Yocto based build flow first install the SDK. The following is an example for Petalinux 2023.1:

```sh
<installer-download-dir>/petalinux-glibc-x86_64-petalinux-image-minimal-cortexa72-cortexa53-zynqmp-generic-xczu19eg-toolchain-2023.1.sh
```

By default the SDK gets installed to /opt/petalinux/2023.1, but any other folder is fine too.

Now prepare the SDK to build kernel modules (use -P for the cd command to resolve symlinks). You will need write permissions on the src/kernel folder, so if your SDK is installed to /opt/petalinux make sure to chown it first!

```sh
source /opt/petalinux/2023.1/environment-setup-cortexa72-cortexa53-xilinx-linux
cd -P /opt/petalinux/2023.1/sysroots/cortexa72-cortexa53-xilinx-linux/usr/src/kernel
make scripts prepare
```

Something is broken with this makefile, it seems to run endlessly. As soon as you see the first warning message like this:

```
scripts/Makefile.build:440: warning: overriding recipe for target 'modules.order'
Makefile:1903: warning: ignoring old recipe for target 'modules.order'
```

You can abort the build with CTRL+C.

Now go back to this project, configure `KERNEL_SRC` and build:

```sh
cd <clone-dir>/linux
export KERNEL_SRC=/opt/petalinux/2023.1/sysroots/cortexa72-cortexa53-xilinx-linux/usr/src/kernel
make
```

To quickly deploy the built module, copy it to the board via scp and do a reboot (unloading the module will not be possible since other modules depend on it):

```sh
scp irq-dct-irc5.ko root@192.168.1.2:/lib/modules/6.1.5-xilinx-v2023.1/extra/irq-dct-irc5.ko && ssh -t root@192.168.1.2 "reboot"
```

Or, if you have booted via NFS, copy it to the NFS share and do a reboot:

```sh
sudo cp -f irq-dct-irc5.ko /export/tysom-zukimo-vv/lib/modules/6.1.5-xilinx-v2023.1/extra/irq-dct-irc5.ko && ssh -t root@192.168.1.2 "reboot"
```

For NFS copy a make target was added, so you can run:

```sh
make deploy
```
