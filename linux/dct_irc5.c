/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 * Based on the Marvell Orion Bridge IRQ driver.
 */

#define DEBUG

#include <linux/irq.h>
#include <linux/irqchip.h>
#include <linux/irqchip/chained_irq.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <linux/of_irq.h>
#include <linux/clk.h>

/* Register offsets */
#define REG_ISM \
	0x00 /* Interrupt Status Mask - Only interrupts	which are set in this mask will fire */
#define REG_RIS \
	0x04 /* Raw Interrupt Status - Unmasked interrupt status, shows active IRQ lines */
#define REG_MIS \
	0x08 /* Masked Interrupt Status - Shows active masked IRQ lines */
#define REG_ISC 0x0C /* Interrupt Status Clear - Clear active interrupt lines */
#define REG_ISS 0x10 /* Interrupt Status Set - Force-set interrupt lines */

static void irc5_irq_handler(struct irq_desc *desc)
{
	struct irq_chip *chip = irq_desc_get_chip(desc);
	struct irq_domain *domain = irq_desc_get_handler_data(desc);
	struct irq_chip_generic *generic_chip =
		irq_get_domain_generic_chip(domain, 0);
	u32 active, hwirq;

	chained_irq_enter(chip, desc);

	/* Get active interrupt lines */
	active = irq_reg_readl(generic_chip, REG_MIS) &
		 generic_chip->mask_cache;

	/* Loop until all interrupts are processed */
	while (active) {
		/* Process interrupts starting with the first (least
		 * significant) bit. This causes interrupts lines with
		 * a smaller index to have a higher priority. */
		hwirq = ffs(active) - 1;
		generic_handle_domain_irq(domain,
					  generic_chip->irq_base + hwirq);
		active &= ~(1 << hwirq);
	}

	chained_irq_exit(chip, desc);
}

static int irc5_irq_init(struct device_node *this,
				struct device_node *parent)
{
	unsigned int clr = IRQ_NOREQUEST | IRQ_NOPROBE | IRQ_NOAUTOEN;
	struct irq_domain *domain;
	struct irq_chip_generic *generic_chip;
	struct platform_device *pdev;
	int ret, irq, num_irqs;

	/* Get number of used interrupt lines from device tree */
	ret = of_property_read_u32(this, "dct,num-irq-lines", &num_irqs);
	if (ret < 0) {
		pr_err("%pOFn: Failed to read 'dct,num-irq-lines' property\n",
		       this);
		return -EINVAL;
	}

	pdev = of_find_device_by_node(this);
	of_node_put(this);
	if (!pdev) {
		pr_err("failed to get platform device for %pOFn\n", this);
	} else {
		struct clk *clk = devm_clk_get_enabled(&pdev->dev, "irc5_clk");
		if (IS_ERR(clk)) {
			dev_err(&pdev->dev,
				"failed to retrieve clk for %pOFn\n", this);
		}
		put_device(&pdev->dev);
	}

	/* Add interrupt domain */
	domain = irq_domain_add_linear(this, num_irqs, &irq_generic_chip_ops,
				       NULL);
	if (!domain) {
		pr_err("%pOFn: Failed to add IRQ domain\n", this);
		return -EACCES;
	}

	/* Allocate generic IRQ chip in the newly created domain and get it */
	ret = irq_alloc_domain_generic_chips(domain, num_irqs, 1, this->name,
					     handle_edge_irq, clr, 0,
					     IRQ_GC_INIT_MASK_CACHE);
	if (ret) {
		pr_err("%pOFn: Failed to allocate generic IRQ chip\n", this);
		return ret;
	}
	generic_chip = irq_get_domain_generic_chip(domain, 0);

	/* Map register space and store base pointer */
	generic_chip->reg_base = of_iomap(this, 0);
	if (!generic_chip->reg_base) {
		pr_err("%pOFn: Failed to map register space\n", this);
		return -ENXIO;
	}

	/* Setup the generic IRQ chip */
	generic_chip->chip_types[0].type = IRQ_TYPE_EDGE_RISING;
	generic_chip->chip_types[0].regs.ack = REG_ISC;
	generic_chip->chip_types[0].regs.mask = REG_ISM;
	generic_chip->chip_types[0].chip.irq_ack = irq_gc_ack_set_bit;
	generic_chip->chip_types[0].chip.irq_mask = irq_gc_mask_clr_bit;
	generic_chip->chip_types[0].chip.irq_unmask = irq_gc_mask_set_bit;

	/* Disable and clear all interrupts */
	irq_reg_writel(generic_chip, 0, REG_ISM);
	irq_reg_writel(generic_chip, (1 << num_irqs) - 1, REG_ISC);

	/* Map the parent interrupt and set the chained handler */
	irq = irq_of_parse_and_map(this, 0);
	if (irq <= 0) {
		pr_err("%pOFn: Failed to map parent IRQ\n", this);
		return -EINVAL;
	}
	irq_set_chained_handler_and_data(irq, irc5_irq_handler, domain);

	pr_info("%pOFf: irc5 IRQ controller initialized for %d lines\n", this,
		num_irqs);

	return 0;
}

IRQCHIP_PLATFORM_DRIVER_BEGIN(irc5)
IRQCHIP_MATCH("dct,irc5", irc5_irq_init)
IRQCHIP_PLATFORM_DRIVER_END(irc5)
MODULE_DESCRIPTION("DCT IRC5 driver");
MODULE_LICENSE("GPL v2");
